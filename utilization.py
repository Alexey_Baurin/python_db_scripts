import fdb
import pymysql
import pandas as pd

# Вписать пароли (взять из файла) или передать как параметры на вход скрипта
# Нужно реализовать механизм приема параметров
fdb_pass=''
x_pass=''
isLocal=True # What database - local or another ?
fdb_name=''
fdb_host=''
x_name=''
x_host=''


'''
mysqlcon = pymysql.connect(
    host='localhost',
    user='root',
    password=x_pass,
    db=''
    #autocommit=True
)

'''

mysqlcon.autocommit(True)
mysqlcur = mysqlcon.cursor()
#mysqlcur.execute("select * from aircompany")
#aircompany_df=pd.DataFrame(mysqlcur)
#print(pd.DataFrame.from_records(iter(mysqlcur), columns=[x[0] for x in mysqlcur.description]))
query="select id, name as op_name from aircompany"
aircompany_df = pd.read_sql(query, mysqlcon)

fbcon = fdb.connect(database=fdb_host+'/3052://firebird/data/'+fdb_name,user='sysdba',password=fdb_pass,charset='WIN1251')

# 1 ЧАСТЬ. Наработки ВС.
# a. Заносим новые авиакомпании, если есть
query="select name from airline"
airline_df_new = pd.read_sql(query, fbcon)
airline_df_new=pd.merge(airline_df_new, aircompany_df, left_on="NAME", right_on="op_name", how='left').sort_values(['id']).reset_index(drop=True)
#airline_df_new=airline_df_new.fillna( {'id':airline_df_new.index}   )

diff_value = airline_df_new.id.max() - airline_df_new.id.idxmax()
for index, row in airline_df_new.iterrows():
    if pd.isna(row['id']):
        airline_df_new.id[index]=index+diff_value

for index, row in airline_df_new.iterrows():
    if pd.isna(row['op_name']):
        paramList=str('id')+",'"+str(row['NAME']+"'")
        paramList="INSERT INTO isdap_x.aircompany VALUES("+paramList+") ON DUPLICATE KEY UPDATE `id` = `id` + 1;"
        mysqlcur.execute(paramList)

# b. Заносим новые самолеты, если находим (с подготовкой)
# Выборка заново обновленных авиакомпаний
query="select id as comp_id, name as op_name from aircompany"
aircompany_df = pd.read_sql(query, mysqlcon)

# Возможно Нужна проверка принадлежности уже занесенных бортов правильной авиакомпании на основе данных из FBD ???
# Выборка самолетов на текущий момент
query="select ac.id, ac.reg, aircomp.id as comp_id from aircraft ac join aircompany aircomp on ac.aircompany_id=aircomp.id"
aircraft_df=pd.read_sql(query, mysqlcon)
query="select ac.sn, ac.reg_number, comp.name as comp_name from aircraft ac join airline comp on ac.id_airline=comp.id"
aircraft_df_new = pd.read_sql(query, fbcon).drop_duplicates('REG_NUMBER')
# Клеим обновленные компании
aircraft_df_new=pd.merge(aircraft_df_new, aircompany_df, left_on="COMP_NAME", right_on="op_name", how='left')
# Клеим полученные выше самолеты (сортируем, выставляем новые индексы)
aircraft_df_new=pd.merge(aircraft_df_new, aircraft_df, left_on="REG_NUMBER", right_on="reg", how='left').sort_values(['id']).reset_index(drop=True)
# FOR TEST
#aircraft_df_new=aircraft_df_new.fillna( {'id':aircraft_df_new.index}   )
#aircraft_df_new=aircraft_df_new.apply(lambda x: x.fillna(aircraft_df_new.id.shift()+1)).ffill()

# Разница между максимальным индексом и максимальным значением поля id
diff_value = aircraft_df_new.id.max() - aircraft_df_new.id.idxmax()
# Первым проходом дозаполняем столбец id у тех элементов где он не существует (NaN)
for index, row in aircraft_df_new.iterrows():
    if pd.isna(row['id']):
        aircraft_df_new.id[index]=index+diff_value
    # Некая проверка уже существующих бортов на корректность принадлежности к авиакомпании
    #elif row['comp_id_x']!=row['comp_id_y']:
    #    print("SN - "+row['SN'])
# Вторым проходом заносим записи с новыми идентификаторами (можно все в один проход и через Лямбды, без циклов)
for index, row in aircraft_df_new.iterrows():
    if pd.isna(row['reg']):
        paramList=str('id')+",'"+str(row['SN'])+"','"+str(row['REG_NUMBER'])+"',2,"+str(row['comp_id_x'])+",'ГТЛК', '' "
        paramList="INSERT INTO isdap_x.aircraft VALUES("+paramList+") ON DUPLICATE KEY UPDATE `id` = `id` + 1;"
        mysqlcur.execute(paramList)

# c. Готовим данные по всем наработкам всех ВС и заносим их в таблицу
# Заново получим таблицу авиакомпаний
query="select id, name as op_name from aircompany"
aircompany_df = pd.read_sql(query, mysqlcon)
    
query="select fhfc.ds,fhfc.df,fhfc.fh,fhfc.fc,fhfc.fh_total,fhfc.fc_total, ac.reg_number,op.name from GT_HC_AC_BY_MMACAL('01.01.2010','01.12.2019') fhfc JOIN AIRCRAFT ac ON fhfc.id_aircraft=ac.id JOIN AIRLINE op ON fhfc.id_airline=op.id"
df_fhfc = pd.read_sql(query, fbcon)
print("end select!\n")
# Присоединяем самолеты с авиакомпаниями
df_fhfc=pd.merge(df_fhfc, aircompany_df, left_on="NAME", right_on="op_name", how='left')
df_fhfc=pd.merge(df_fhfc, aircraft_df, left_on="REG_NUMBER", right_on="reg", how='left')
#df_fhfc = pd.merge(df_fhfc, aircraft_df[['reg','REG_NUMBER']],on='REG_NUMBER', how='left') # если есть индексы по соотв. полям
print("START INSERT!\n")

# Коэфф. домножения на минуты, так как в базе X - минуты вместо часов
coeff_minutes=60
for index, row in df_fhfc.iterrows():
    #print( index+1, )
    paramList=str(index+1)+","+str(row['id_y'])+", NULL, NULL, '"+str(row['DS'])+"','"+str(row['DF'])+" 23:59:59',"+str(row['FH']*coeff_minutes)+","+str(row['FH_TOTAL']*coeff_minutes)+","+str(row['FC'])+","+str(row['FC_TOTAL'])+",2,"+str(row['id_x'])
    paramList="INSERT INTO isdap_x.nalet VALUES("+paramList+") ON DUPLICATE KEY UPDATE `id` = `id` + 1;"
    # Проверка на дубль в авиакомпании
    #if not pd.isna(row['id_y']) and row['id_x']==3 : #not (row['id_y']>=0):
    #    print(index, row['id_y'], row['id_x'], row['NAME'], row['REG_NUMBER'] ) 
    mysqlcur.execute(paramList)
    #mysqlcon.commit() # не нужен - выше включен автокоммит

# 2 ЧАСТЬ. Блок Главных элементов - перенести в конец !

query="select id as id_ac, reg as reg_ac from aircraft"
aircraft_df=pd.read_sql(query, mysqlcon)

query="select un.sn, untp.model from unit un join unit_type untp on un.id_unit_type=untp.id where sn!='' and sn!='RE 220(RJ)\nWE380'" # очистку делать на Пандасе
unit_sn_pn = pd.read_sql(query, fbcon)
query="select id, serial from component where type=1"
units_old = pd.read_sql(query, mysqlcon)
unit_sn_pn=pd.merge(unit_sn_pn, units_old, left_on="SN",right_on="serial",how='left').sort_values('id').reset_index(drop=True)
unit_sn_pn_max_id = pd.read_sql("select max(id) as id from component", mysqlcon)['id'][0]
diff_value = unit_sn_pn_max_id - unit_sn_pn.id.idxmax()
for index, row in unit_sn_pn.iterrows():
    if pd.isna(row['id']):
        unit_sn_pn.id[index]=index+diff_value
# Добавим отсутствующие элементы
for index, row in unit_sn_pn.iterrows():
    if pd.isna(row['serial']):
        paramList=str('id')+",1,'"+str(row['MODEL'])+"','"+str(row['SN'])+"','"+str(row['SN'])+"', '', 'ГТЛК'"
        #print("comp - ", index)
        paramList="INSERT INTO isdap_x.component VALUES("+paramList+") ON DUPLICATE KEY UPDATE `id` = `id` + 1;"
        mysqlcur.execute(paramList)

# Облегчим датафрейм, удалив лишние столбцы
unit_sn_pn.drop(['MODEL','serial'], inplace=True, axis=1)

print("REQUEST SQL:")
query="select fhfc.ds,fhfc.df,fhfc.fh,fhfc.fc,fhfc.fh_total,fhfc.fc_total, fhfc.unit_place, fhfc.unit_sn, fhfc.id_aircraft, ac.reg_number from GET_ALL_ID_UNIT_BY_MM fhfc JOIN AIRCRAFT ac ON fhfc.id_aircraft=ac.id where unit_place<>0 and unit_sn<>'нет данных' and unit_sn<>'не установлен'"
df_unit_fhfc = pd.read_sql(query, fbcon)
print("MERGE_1:")
df_unit_fhfc=pd.merge(df_unit_fhfc, unit_sn_pn, left_on="UNIT_SN",right_on="SN",how='left').reset_index(drop=True)
df_unit_fhfc=pd.merge(df_unit_fhfc, aircraft_df, left_on="REG_NUMBER",right_on="reg_ac",how='left').sort_values('id').reset_index(drop=True)

coeff_minutes=60
unit_place="0"
for index, row in df_unit_fhfc.iterrows():
    #print("OPER - ", index+1 )
    if row['UNIT_PLACE']==20:
        unit_place=str(3)
    else:
        unit_place=str(row['UNIT_PLACE'])
    paramList=str(index+1)+","+str(row['id'])+", '"+str(row['DS'])+"','"+str(row['DF'])+" 23:59:59',"+str(row['FH']*coeff_minutes)+","+str(row['FH_TOTAL']*coeff_minutes)+","+str(row['FC'])+","+str(row['FC_TOTAL'])+","+str(row['id_ac'])+", "+unit_place
    paramList="INSERT INTO isdap_x.oper_time VALUES("+paramList+") ON DUPLICATE KEY UPDATE `id` = `id` + 1;"
    mysqlcur.execute(paramList)
    #mysqlcon.commit()

fbcon.close()
mysqlcon.close()